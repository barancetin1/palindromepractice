package sheridan;

public class Palindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(isPalindrome("anne"));

	}
	static boolean isPalindrome(String s) {
		s = s.toUpperCase( ).replaceAll(" ", "");
		
		for(int i = 0, j = s.length()-1; i<j  ;i++, j--) {
			if(s.charAt( i ) != s.charAt( j )) {
				return false;
			}
		}
		return true;
	}

}
