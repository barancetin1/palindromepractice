package sheridan;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}
	
	@Test
	public void testIsPalindrome() {
		assertTrue("invalid value for palindrome" , Palindrome.isPalindrome(" Anna "));
	}
	
	@Test
	public void testPalinBoundaryIn() {
		assertTrue("invalid val for pal", Palindrome.isPalindrome("a"));
	}
	
	@Test
	public void testPalinBoundaryOut() {
		assertFalse("invalid val for pal", Palindrome.isPalindrome("arna"));
	}
	
	@Test
	public void testPalinNegativeException() {
		assertFalse("invalid val for pal", Palindrome.isPalindrome("abc"));
	}

}
